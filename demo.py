import numpy as np
import scipy as sp
from matplotlib import pylab as pl

np.random.seed(1234)

def sigmoid(x):
    return 1./(1+np.exp(-x))

n_arms = 3
n_steps = 5000
eps_g_i = 1.
rho = 0.2

current_mean = np.zeros(n_arms)
true_mean = np.random.normal(0,1,n_arms)

mu_star = sigmoid(np.max(true_mean))
i_star = np.argmax(true_mean)


mask = np.ones(n_arms, dtype = bool)
mask[i_star] = False
delta = np.min(mu_star-sigmoid(true_mean[mask]))

reward = np.zeros(4)
regret = np.zeros((4,n_steps))

visits = np.zeros((4,n_arms))
emp_gain = np.zeros((4,n_arms))

ucb = np.zeros(n_arms)
ucbr = np.zeros(n_arms)

for t in range(n_steps):
        
    gain = np.random.normal(true_mean,1)
    gain = sigmoid(gain)
    
    if t<50:
        it_random = np.random.choice(np.arange(1,n_arms))
        for i in range(4):
            reward[i] += gain[it_random]
            regret[i,t] = (t+1)*mu_star - reward[i]
            visits[i,it_random] += 1
        continue

    #Random strategy
    
    it_random = np.random.choice(np.arange(1,n_arms))
    
    reward[0] += gain[it_random]
    regret[0,t] = (t+1)*mu_star - reward[0]

    #eps greedy
    eps_g = min(6*n_arms/(delta**2*(t+1)),1)
    #eps_g = 100*eps_g_i/(t+1)

    draw = np.random.uniform(0,1)
    if draw<eps_g or t==0: it_greedy = np.random.choice(np.arange(1,n_arms))
    else: it_greedy = np.nanargmax(np.divide(emp_gain[1,:],visits[1,:]))
    
    visits[1,it_greedy] += 1
    emp_gain[1,it_greedy]+= gain[it_greedy]

    reward[1] += gain[it_greedy]
    regret[1,t] += (t+1)*mu_star - reward[1]

    #With UCB1
    if t==0: it_ucb = np.random.choice(np.arange(1,n_arms))
    else:
        it_ucb = np.nanargmax(np.divide(emp_gain[2,:],visits[2,:])+np.sqrt(np.divide(2*np.log(t+1),visits[2,:])))
    visits[2,it_ucb] += 1
    emp_gain[2,it_ucb]+= gain[it_ucb]

    reward[2] += gain[it_ucb]
    regret[2,t] += (t+1)*mu_star - reward[2]

    #With UCB rho
    if t==0: it_ucb = np.random.choice(np.arange(1,n_arms))
    else: it_ucb = np.nanargmax(np.divide(emp_gain[3,:],visits[3,:])+np.sqrt(np.divide(rho*np.log(t+1),visits[3,:])))

    visits[3,it_ucb] += 1
    emp_gain[3,it_ucb]+= gain[it_ucb]

    reward[3] += gain[it_ucb]
    regret[3,t] += (t+1)*mu_star - reward[3]    
    

pl.plot(regret[0,:], 'b')
pl.plot(regret[1,:], 'r')
pl.plot(regret[2,:], 'g')
pl.plot(regret[3,:], 'brown')
pl.legend([r'Random', r'Greedy', r'UCB1', r'UCBr'], loc = 'upper left')
pl.show()
